Plain-circle: realease notes
============================

1.00 - 8 Nov 2016
------------------
- Imported Theme `Plain`
- Changed drop shadow to circle with darker edge
- Add first screenshots


---------------
E. Frank Sandig
2016-11-08
