Plain-circle
============


Simple dark theme for cairo-clock
---------------------------------
Based on Plain, which was just a dark square.

Unzip and copy to ~/.cairo-clock/themes/

Current version is 1.00 from 2016-11-08.

Tested with MacSlow's Cairo-Clock 0.3.4 in Ubuntu 16.04 LTS, see folder "Screenshots".


---------------
E. Frank Sandig
2016-11-06
